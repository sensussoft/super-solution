<footer class="footer text-right">
        <div class="col-sm-12">
        © 2016 Portfolios. All rights reserved.
        </div>
</footer>
</div>
</div>

<script>
    var resizefunc = [];
</script> 
 
{{ HTML::script('assets/js/jquery.min.js') }}        
{{ HTML::script('assets/js/bootstrap.min.js') }}        
{{ HTML::script('assets/js/jquery.app.js') }}

@yield('script')
</body>
</html>