@include('layout.header')
@include('layout.sidebar')

    <!-- Start content -->

    <div class="content-page">
	    <div class="content">
	        <div class="container">
	         	@yield('content')
	        </div> <!-- container -->
	    </div> <!-- content -->
    </div>

@include('layout.footer')